# Split Landing Page

A landing page for a couple of products.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).

Images (c) Sony and Microsoft
